const Sequelize = require('sequelize');
const {gt, lte, ne, in: opIn} = Sequelize.Op;
const {Database, db_info} = require('../database');

var sequelize = new Sequelize(
        db_info.database, 
        db_info.user, 
        db_info.pass,
        {
            host: db_info.host,
            port: db_info.port,
            dialect: 'mysql',
            logging: false, //TODO: update with logging
            pool: {
                max:5,
                min: 0,
                acquire: 30000,
                idle: 10000
            } 
    });

/* Sequelize Structures */
const GuestbookEntry = sequelize.define('guestbook_entry', {
    nickname: Sequelize.STRING,
    email: Sequelize.STRING,
    message: Sequelize.STRING
});

sequelize.sync(); //Create tables

async function addEntry(nickname="", email="", message="") {
    GuestbookEntry.create(nickname, email, message).then(res => console.log('Entry logged'));
    return {nickname, email, message};
}

async function getEntries() {
    sequelize.sync();
    return await GuestbookEntry.findAll({
        where: {
            message: {
                [ne]: '' || null
            }
        }
    });
}


module.exports = {
    addEntry,
    getEntries
}