/** @author "Zachary Gillis"
 *  @name = "Database"
 */
// COnstants
const {db_info} = require('./config.js');
/* Sequelize */
var Sequelize = require('sequelize');
var sequelize = SetUp();

function SetUp() {
     let sequelize = new Sequelize(
        db_info.database, 
        db_info.user, 
        db_info.pass,
        {
            host: db_info.host,
            port: db_info.port,
            dialect: 'mysql',
            logging: false, //TODO: update with logging
            pool: {
                max:5,
                min: 0,
                acquire: 30000,
                idle: 10000
            } 
    });
    return sequelize;
};

function GetDataBaseORM() {
    SetUp();
    return sequelize;
}

function execSql(sql) {
    
}




// sequelize.


module.exports = {
    sequelize,
    db_info
};