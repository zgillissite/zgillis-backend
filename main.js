const express = require('express');
const bodyParser = require('body-parser');
const database = require('./database');
const GuestbookEntry = require('./entity/GuestbookEntry');

const app = express();
/* PORT FOR SERVER */
const PORT = 3050;


console.log(`ZGILLIS.COM BACKEND SERVER\n`);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParser.json());



/* Endpoint Definitions */
app.post('/guestbook/add', async function (req, res) {
    res.contentType('application/json')
    let incoming = req.body;

    if(incoming.nickname && incoming.email && incoming.message) {
        await GuestbookEntry.addEntry({
            nickname: incoming.nickname || '',
            email: incoming.email,
            message: incoming.message
        });
        let response = {
            date: new Date().getDate(),
            time: new Date().getTime(),
            code: "Success",
            entry_request: incoming
        }
        res.send(response);
    } else {
        res.send({message: 'You need to fill out all params.'});
    }
});

app.get('/guestbook/all', async function (req, res) {
    res.contentType('application/json')
    let data = await GuestbookEntry.getEntries();
    res.send(data);
});




app.listen(PORT, () => {
    console.log('Listening on port ' + PORT);
});


