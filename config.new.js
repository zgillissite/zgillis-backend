/* CONFIGURATION FILE */
/* Node: edit this and rename config.js */

// Configure your MySQL database credentials
let db_info = {
    host:   'localhost',
    port: 3306,
    user:   'root',
    pass:   'pass',
    database: 'database_name'
};

module.exports.db_info = db_info;